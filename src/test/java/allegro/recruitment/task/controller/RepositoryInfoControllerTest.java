package allegro.recruitment.task.controller;

import allegro.recruitment.task.ServerStarter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import static org.hamcrest.Matchers.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerStarter.class)
@WebAppConfiguration
public class RepositoryInfoControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getRepositoryInfo() throws Exception {
        mockMvc.perform(get("/repositories/dominik-burdzy/gethere"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.full_name", is("dominik-burdzy/GeThere")))
                .andExpect(jsonPath("$.description", isEmptyOrNullString()))
                .andExpect(jsonPath("$.clone_url", is("https://github.com/dominik-burdzy/GeThere.git")))
                .andExpect(jsonPath("$.stargazers_count", is(0)))
                .andExpect(jsonPath("$.created_at", is("2016-04-09T23:25:29Z")));
    }

}