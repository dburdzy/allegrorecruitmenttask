package allegro.recruitment.task.client;

public class GithubInvocationException extends RuntimeException {

    public GithubInvocationException(String message) {
        super(message);
    }
}
