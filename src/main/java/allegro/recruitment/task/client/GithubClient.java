package allegro.recruitment.task.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GithubClient {

    public String get(String path) {
        Client client = Client.create();
        WebResource webResource = client.resource(path);
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new GithubInvocationException("Failed to GET: " + webResource.getURI() + ". HTTP error code : " + response.getStatus());
        }
        return response.getEntity(String.class);
    }

}
