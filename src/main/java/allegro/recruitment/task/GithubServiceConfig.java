package allegro.recruitment.task;

import allegro.recruitment.task.api.GithubAPI;
import allegro.recruitment.task.api.RepositoryInfoJsonParser;
import allegro.recruitment.task.client.GithubClient;
import allegro.recruitment.task.controller.RepositoryInfoController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GithubServiceConfig {

    @Bean
    public RepositoryInfoJsonParser repositoryInfoJsonParser() {
        return new RepositoryInfoJsonParser();
    }

    @Bean
    public GithubClient githubClient() {
        return new GithubClient();
    }

    @Bean
    public GithubAPI githubAPI(GithubClient githubClient, RepositoryInfoJsonParser repositoryInfoJsonParser) {
        return new GithubAPI(githubClient, repositoryInfoJsonParser);
    }

    @Bean
    public RepositoryInfoController repositoryInfoController(GithubAPI githubAPI) {
        return new RepositoryInfoController(githubAPI);
    }

}
