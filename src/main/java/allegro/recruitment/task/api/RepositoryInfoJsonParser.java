package allegro.recruitment.task.api;

import allegro.recruitment.task.model.RepositoryInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RepositoryInfoJsonParser {

    private final Gson gson;

    public RepositoryInfoJsonParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gson = gsonBuilder.create();
    }

    public RepositoryInfo parse(String issueJson) {
        return gson.fromJson(issueJson, RepositoryInfo.class);
    }
}
