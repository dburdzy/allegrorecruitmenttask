package allegro.recruitment.task.api;

import allegro.recruitment.task.client.GithubClient;
import allegro.recruitment.task.configuration.GithubAPIConfiguration;
import allegro.recruitment.task.model.RepositoryInfo;

public class GithubAPI {

    private static final String GITHUB_API_URL = "https://api.github.com/repos/";

    private GithubClient githubClient;
    private RepositoryInfoJsonParser repositoryInfoJsonParser;

    public GithubAPI(GithubClient githubClient, RepositoryInfoJsonParser repositoryInfoJsonParser) {
        this.githubClient = githubClient;
        this.repositoryInfoJsonParser = repositoryInfoJsonParser;
    }

    public RepositoryInfo getRepositoryInfo(GithubAPIConfiguration githubAPIConfiguration) {
        StringBuilder sb = new StringBuilder(GITHUB_API_URL);
        sb.append(githubAPIConfiguration.getOwner());
        sb.append("/");
        sb.append(githubAPIConfiguration.getRepositoryName());

        return repositoryInfoJsonParser.parse(githubClient.get(sb.toString()));
    }
}
