package allegro.recruitment.task.model;

public class RepositoryInfo {

    private String full_name;
    private String description;
    private String clone_url;
    private int stargazers_count;
    private String created_at;

    public RepositoryInfo() {
    }

    public RepositoryInfo(String full_name, String description, String clone_url, int stargazers_count, String created_at) {
        this.full_name = full_name;
        this.description = description;
        this.clone_url = clone_url;
        this.stargazers_count = stargazers_count;
        this.created_at = created_at;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClone_url() {
        return clone_url;
    }

    public void setClone_url(String clone_url) {
        this.clone_url = clone_url;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
