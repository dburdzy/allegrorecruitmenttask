package allegro.recruitment.task.configuration;

public class GithubAPIConfiguration {

    private String owner;
    private String repositoryName;

    public GithubAPIConfiguration(String owner, String repositoryName) {
        this.owner = owner;
        this.repositoryName = repositoryName;
    }

    public String getOwner() {
        return owner;
    }

    public String getRepositoryName() {
        return repositoryName;
    }
}
