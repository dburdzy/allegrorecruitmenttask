package allegro.recruitment.task.controller;

import allegro.recruitment.task.api.GithubAPI;
import allegro.recruitment.task.configuration.GithubAPIConfiguration;
import allegro.recruitment.task.model.RepositoryInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/repositories")
public class RepositoryInfoController {

    private GithubAPI githubAPI;

    public RepositoryInfoController(GithubAPI githubAPI) {
        this.githubAPI = githubAPI;
    }

    @RequestMapping(value = {"/{owner}/{repositoryName}"}, method= RequestMethod.GET)
    public @ResponseBody
    RepositoryInfo getRepositoryInfo(
            @PathVariable("owner") String owner,
            @PathVariable("repositoryName") String repositoryName) {

        GithubAPIConfiguration githubAPIConfiguration = new GithubAPIConfiguration(owner, repositoryName);
        return githubAPI.getRepositoryInfo(githubAPIConfiguration);
    }
}
